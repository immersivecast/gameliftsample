// Fill out your copyright notice in the Description page of Project Settings.


#include "MyFirstGameLiftGameState.h"
#include "Net/UnrealNetwork.h"

void AMyFirstGameLiftGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AMyFirstGameLiftGameState, LatestEvent);
	DOREPLIFETIME(AMyFirstGameLiftGameState, WinningTeam);

}
